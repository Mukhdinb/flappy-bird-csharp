﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Flappy_Bird_dotnet
{
    public partial class Form1 : Form
    {
        int pipeSpeed = 7;
        int gravity = 10;
        int score = 0;
        Boolean end_game=false;

        public Form1()
        {
            InitializeComponent();
        }

        private void gameTimerEvent(object sender, EventArgs e)
        {
            Bird.Top += gravity;
            PipeBottom.Left -= pipeSpeed;
            PipeTop.Left -= pipeSpeed;
            ScoreText.Text = "Score: "+score.ToString();
            

            if (PipeTop.Left < -50)
            {
                PipeTop.Left = 850;
                score++;
            }
            if (PipeBottom.Left < -40)
            {
                PipeBottom.Left = 780;
                score++;
            }

            if(Bird.Bounds.IntersectsWith(PipeTop.Bounds) ||
               Bird.Bounds.IntersectsWith(PipeBottom.Bounds) ||
               Bird.Bounds.IntersectsWith(Ground.Bounds) ||
               Bird.Top<-20)
            {
                endGame();
            }
        }

        private void gameKeyIsDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                gravity = -10;
            }
            if(end_game && e.KeyCode== Keys.R)
            {
                end_game = false;
                GameTimer.Start();
                score = 0;
                Bird.Top = 175;
                Bird.Left = 185;
            }

        }

        private void gameKeyIsUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                gravity = 10;
            }
        }

        private void endGame()
        {
            GameTimer.Stop();
            ScoreText.Text += " Game Over! Press R to restart";
            end_game = true;
            
        }
    }
}
